# Binance Node

[Binance full node docs](https://docs.binance.org/fullnode.html#run-full-node-to-join-binance-chain)
[Binance full node repo](https://github.com/binance-chain/node-binary)

Docker image for Binance fullnode/lightnode mainnet/testnet
